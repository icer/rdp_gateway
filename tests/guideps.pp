#installs base dependencies for testing on a minimal CentOS-like system
# This is opinionated, and probably shouldn't be used.

exec { 'yum Group Install':
  unless  => '/usr/bin/yum grouplist "General Purpose Desktop" | /bin/grep "^Installed Groups"',
  command => '/usr/bin/yum -y groupinstall "General Purpose Desktop"',
}

# other stuff needed to make a functioning gnome session.
package { 'gnome-session-xsession':
  ensure  => installed,
  require => Exec['yum Group Install'],
}
package { 'gnome-applets':
  ensure  => installed,
  require => Exec['yum Group Install'],
}
package { 'gnome-menus':
  ensure  => installed,
  require => Exec['yum Group Install'],
}

package { 'gnome-games':
  ensure  => installed,
  require => Exec['yum Group Install'],
}


