require 'spec_helper'

describe 'rdp_gateway' do
  describe 'internal' do
    it { should contain_class('rdp_gateway::install') }
    it { should contain_class('rdp_gateway::config').with(
      :require => "Class[Rdp_gateway::Install]"
    ) }
  end
end
