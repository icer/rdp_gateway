# configure the things
class rdp_gateway::config {
  file { '/usr/share/xrdp/ad24b.bmp':
    ensure => present,
    source => "puppet:///modules/${module_name}/gui_files/ad24b.bmp",
  }  # ICER Slash Logos.

  file { '/usr/share/xrdp/ad256.bmp':
    ensure => present,
    source => "puppet:///modules/${module_name}/gui_files/ad256.bmp",
  }  # ICER Slash Logos.

  file { '/usr/share/xrdp/xrdp24b.bmp':
    ensure => present,
    source => "puppet:///modules/${module_name}/gui_files/xrdp24b.bmp",
  }  # ICER Slash Logos.

  ##
  file { '/usr/share/backgrounds/xrdp256.bmp':
    ensure => present,
    source => "puppet:///modules/${module_name}/gui_files/xrdp256.bmp",
  }  # ICER Slash Logos.

  # default background
  file { '/usr/share/backgrounds/default.xml':
    ensure => present,
    source => "puppet:///modules/${module_name}/default.xml",
  }

  file { '/usr/share/gnome/autostart/gnome-terminal.desktop':
    ensure => present,
    source => "puppet:///modules/${module_name}/gnome-terminal.desktop",
  } #Open Terminal on the desktop
 
  file { '/etc/skel/Terminal.desktop':
    ensure => present,
    source => "puppet:///modules/${module_name}/Terminal.desktop",
  } #Add Terminal Launcher to the desktop



}
