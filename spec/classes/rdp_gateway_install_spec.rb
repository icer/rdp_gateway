require 'spec_helper'

describe 'rdp_gateway' do
  describe 'install' do
    it { should contain_package('xrdp') }
    it { should contain_package('tigervnc-server') }
  end
end
