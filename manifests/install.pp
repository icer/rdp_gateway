#does the install
class rdp_gateway::install {
  package { 'xrdp':
    ensure => installed,
  }
  package { 'tigervnc-server':
    ensure => installed,
  }

}
