require 'spec_helper'

describe 'rdp_gateway' do
  describe 'config' do
    it { should contain_file('/usr/share/xrdp/ad24b.bmp').with(
      :ensure => 'present',
      :source => 'puppet:///modules/rdp_gateway/gui_files/ad24b.bmp',
    ) }
    it { should contain_file('/usr/share/xrdp/ad256.bmp').with(
      :ensure => 'present',
      :source => 'puppet:///modules/rdp_gateway/gui_files/ad256.bmp'
    ) }
    it { should contain_file('/usr/share/xrdp/xrdp24b.bmp').with(
      :ensure => 'present',
      :source => 'puppet:///modules/rdp_gateway/gui_files/xrdp24b.bmp'
    ) }
    it { should contain_file('/usr/share/backgrounds/xrdp256.bmp').with(
      :ensure => 'present',
      :source => 'puppet:///modules/rdp_gateway/gui_files/xrdp256.bmp'
    ) }
    it { should contain_file('/usr/share/backgrounds/default.xml').with(
      :ensure => 'present',
      :source => 'puppet:///modules/rdp_gateway/default.xml'
    ) }
  end
end
